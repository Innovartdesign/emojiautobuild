const fs = require('fs');
const readline = require('readline');
const rimraf = require("rimraf");
const path = require('path');
const {google} = require('googleapis');
const mime = require('mime');
const delay = require('delay');
const argv = require('minimist')(process.argv.slice(2));
const directoryPath = path.join(__dirname,'google_drive');

const SCOPES = [
    'https://www.googleapis.com/auth/drive.metadata.readonly',
    'https://www.googleapis.com/auth/drive.readonly',
  ]

//--release=...(fid)
//--debug=...(fid)
const token = argv.token;
const root = argv.root;

if( !token ){
  console.log('token missing')
  process.exit(1);
  return;
}

download();

async function download(){
    try{

        console.log(root);

        const drive = await authorize(decode(token));

        if(fs.existsSync(directoryPath)){
            rimraf.sync(directoryPath);
        }

        fs.mkdirSync(directoryPath, { recursive:true });
        await downloadFolder(drive, 0, root, directoryPath);

        console.log("Done! 👍");
        process.exit()

    }catch(err){
        console.log("Oops! ❌",err);
        console.warn(err);
        process.exit(1)
    }
}

function decode(b64string) {
  return JSON.parse(Buffer.from(b64string, 'base64'));
}

async function authorize(credentials) {
  const {client_id, client_email, private_key} = credentials;
  const jwtClient = new google.auth.JWT(client_email, null, private_key,SCOPES);

  await jwtClient.authorize();
  return google.drive({version: 'v3', auth:jwtClient});
}
async function downloadFolder(drive, depth, folder, p){

    let pageToken = null;

    do{
        let res = await drive.files.list({
            q: `'${folder}' in parents`,
            fields: 'nextPageToken, files(id, name, mimeType)',
            spaces: 'drive',
            pageToken: pageToken
        });


        for(let idx in res.data.files){
            await delay(500); // prevent brute force and limited by Google drive.

            let file = res.data.files[idx];
            let filePath = path.join(p,file.name);
            //Is folder
            if(file.mimeType == 'application/vnd.google-apps.folder') {
                console.log(`${'\t'.repeat(depth)}${file.name} Directory`);
                await downloadFolder(drive, depth+1, file.id, filePath);
            }else if(file.mimeType == 'application/vnd.google-apps.spreadsheet'){
                process.stdout.write(`${'\t'.repeat(depth)}${file.name} CSV `);
                await exportSheet(drive, depth+1, file, p);
            }else if(file.name.endsWith('.png') || file.name.endsWith('.emoji')){
                process.stdout.write(`${'\t'.repeat(depth)}${file.name} ${file.mimeType} `);
                await downloadFile(drive, depth+1, file, p);
            }else if(file.mimeType == 'application/json' || file.mimeType == 'text/csv'){
                process.stdout.write(`${'\t'.repeat(depth)}${file.name} ${file.mimeType} `);
                await downloadFile(drive, depth+1, file, p);
            }else{
                console.log('\t'.repeat(depth), 'skip', file.name, file.mimeType);
            }
        }

        if(res.data.nextPageToken){
            console.log('nextPageToken: ', res.data.nextPageToken);                
        }

        pageToken = res.data.nextPageToken;

    }while(pageToken!=null);

}

async function downloadFile(drive, depth, f, p){
    let fileName = f.name;
    let fileId = f.id;

    let fileParent = p;
    let filePath = path.join(p,fileName);

    if(!fs.existsSync(fileParent)){
        // console.log('\t'.repeat(depth), `mkdir ${fileName}`);
        fs.mkdirSync(fileParent, { recursive:true });
    } else if(fs.existsSync(filePath)) {
        // console.warn('\t'.repeat(depth), `${fileName} already exist`);
        return filePath;
    }

    process.stdout.write(`...`);

    let res = await drive.files.get({fileId:fileId, alt: 'media'}, {responseType: 'stream'});
    
    await new Promise((resolve, reject) => {

        let dest = fs.createWriteStream(filePath);

        dest.on('finish', ()=>{
            resolve();
        })

        res.data
            .on('error', err => reject(err))
            .pipe(dest);

    });

    console.log('Done!');

    return filePath;

}

async function exportSheet(drive, depth, f, p){
    let fileName = f.name+'.csv';
    let fileId = f.id;

    let fileParent = p;
    let filePath = path.join(p,fileName);

    if(!fs.existsSync(fileParent)){
        fs.mkdirSync(fileParent, { recursive:true });
    } else if(fs.existsSync(filePath)) {
        return filePath;
    }

    process.stdout.write(`...`);

    let res = await drive.files.export({fileId:fileId, mimeType: 'text/csv'}, {responseType: 'stream'});

    await new Promise((resolve, reject) => {
        
        let dest = fs.createWriteStream(filePath);

        dest.on('finish', ()=>{
            resolve();
        })

        res.data
            .on('error', err => reject(err))
            .pipe(dest);

    });

    console.log('Done!');

    return filePath;
}



