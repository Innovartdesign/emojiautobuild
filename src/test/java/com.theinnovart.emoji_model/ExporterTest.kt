package com.theinnovart.emoji_model

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.theinnovart.emoji_model.model.Manifest
import mu.KotlinLogging
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.io.File

internal class ExporterTest{
    private val gson = GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create()!!
    private val logger = KotlinLogging.logger {}

    @BeforeEach
    fun reset(){
        val src = File("src/test/resources/src", GLOBAL_MANIFEST+".bak")
        val dst = File("src/test/resources/src", GLOBAL_MANIFEST)
        src.copyTo(dst, true)
        val src2 = File("src/test/resources/src", LOCAL_LANG_CSV+".bak")
        val dst2 = File("src/test/resources/src", EMOJI_FOLDER_NAME+File.separator+LOCAL_LANG_CSV)
        src2.copyTo(dst2, true)
    }

    @AfterEach
    fun clear() {
        File("src/test/resources/src", EMOJI_FOLDER_NAME+File.separator+LOCAL_LANG_CSV).delete()
    }

    @Test
    fun read_manifest_write_back() {
        val src = File("src/test/resources/src")
        val dst = File("src/test/resources/dst")
        val build = File("src/test/resources/tmp")
        val expected = File(src, GLOBAL_MANIFEST).readText().let { gson.fromJson(it,
            Manifest::class.java) }
        Exporter(src,dst,build).clear().load().update().export()
        val actual = File(src, GLOBAL_MANIFEST).readText().let { gson.fromJson(it,
            Manifest::class.java) }
        Assertions.assertEquals(expected,actual,"Expected ${expected.toJson(gson)}, but was ${actual.toJson(gson)}")
    }

    @Test
    fun read_manifest_export() {

        File(".").walkTopDown().onEnter {
            logger.debug {
                it.path
            }
            true
        }
        val src = File("src/test/resources/src")
        val dst = File("src/test/resources/dst")
        val build = File("src/test/resources/tmp")
        val expected = File(src, GLOBAL_MANIFEST).readText().let { gson.fromJson(it,
            Manifest::class.java) }
        Exporter(src,dst,build).clear().load().update().export()
        val actual = File(dst, GLOBAL_MANIFEST).readText().let { gson.fromJson(it,
            Manifest::class.java) }
        Assertions.assertEquals(expected,actual,"Expected ${expected.toJson(gson)}, but was ${actual.toJson(gson)}")
    }
}

fun Manifest.toJson(gson: Gson): String {
    return gson.toJson(this)
}
