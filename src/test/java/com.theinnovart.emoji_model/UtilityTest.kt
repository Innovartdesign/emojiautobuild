package com.theinnovart.emoji_model

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.theinnovart.emoji_model.model.EmojiPackageMeta
import com.theinnovart.emoji_model.model.LangEmojiPackageMeta
import mu.KotlinLogging
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.io.File
import java.io.FileWriter

internal class UtilityTest{
    private val logger = KotlinLogging.logger {}
    private val gson = GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create()!!
    private val srcPath = "src/test/resources/src"
    private val dstPath = "src/test/resources/dst"

    @BeforeEach
    fun reset() {
        File("src/test/resources/src", LOCAL_LANG_CSV+".bak")
            .copyTo(File(srcPath, EMOJI_FOLDER_NAME+File.separator+ LOCAL_LANG_CSV), overwrite = true)
    }

    @AfterEach
    fun clear() {
        File(srcPath, EMOJI_FOLDER_NAME+File.separator+ LOCAL_LANG_CSV).delete()
        File(dstPath).deleteRecursively()
    }

    @Test
    fun get_official_next_version_ground(){
        val official = EmojiPackageMeta(name = "official")
        val demo = EmojiPackageMeta(name = "demo-mets")
        val test = EmojiPackageMeta(name = "test-color")

        val actual = listOf(
            Utility.getNextPackageVersion(official, listOf(official,demo,test)),
            Utility.getNextPackageVersion(demo, listOf(official,demo,test)),
            Utility.getNextPackageVersion(test, listOf(official,demo,test))
        )
        val expected = listOf(20L,100000L,200000L)
        Assertions.assertEquals(expected, actual)
    }

    @Test
    fun get_official_next_version(){

        val old = mutableListOf(
            EmojiPackageMeta(name = "official").apply { versions = listOf(25,27,29) }
        )

        val new = listOf(
            EmojiPackageMeta(name = "demo-mets"),
            EmojiPackageMeta(name = "demo-wedding"),
            EmojiPackageMeta(name = "demo-wedding"),
            EmojiPackageMeta(name = "test-color"),
            EmojiPackageMeta(name = "official"),
            EmojiPackageMeta(name = "official"),
            EmojiPackageMeta(name = "demo-mets"),
            EmojiPackageMeta(name = "test-other")
        )

        val res = listOf<Long>(100000,100001,100002,200000,30,31,100003,200001)
        old.addAll(new)
        new.forEachIndexed { index, meta ->
            val actual = Utility.getNextPackageVersion(meta, old)
            val expected = res[index]
            Assertions.assertEquals(expected, actual)
            meta.versions = meta.versions + expected
        }
    }


    @Test
    fun generate_lang_emoji_package_meta_from_ground(){
        val src = File(srcPath)
        val actual = Utility.generateLangEmojiPackageMeta(src)
        val expected = setOf(
            LangEmojiPackageMeta(
                lang = "en-US",
                packages = listOf(
                    EmojiPackageMeta(
                        name = "demo-mets",
                        lang = "en-US",
                        metaDigest = "0cb79cc089d48d1abffb2e10eac2ab79",
                        filesDigest = "7ee6fb9bf5ac1b8bc8c56c948e462873",
                        builds = listOf("debug","release","staging")
                    )
                ),
                trafficOrder = listOf(19,20,4,1,6,5),
                customOrder = listOf(12,13,11)
            ),
            LangEmojiPackageMeta(
                lang = "ja-JP",
                packages = listOf(
                    EmojiPackageMeta(
                        name = "demo-mets",
                        lang = "ja-JP",
                        metaDigest = "7be07d3fded6fe54c920b00a143ab3d1",
                        filesDigest = "7ee6fb9bf5ac1b8bc8c56c948e462873",
                        builds = listOf("debug","release","staging")
                    ),
                    EmojiPackageMeta(
                        name = "demo-softbank",
                        lang = "ja-JP",
                        metaDigest = "e542f8e3ad72cb5617efdf11188f3ad9",
                        filesDigest = "0cc4a79dfcb36a3fcf973c225d9db45c",
                        builds = listOf("debug","staging")
                    )
                ),
                trafficOrder = listOf(19,31,4,1,6,5),
                customOrder = listOf(33,13,11)
            ),
            LangEmojiPackageMeta(
                lang = "zh-TW",
                packages = listOf(
                    EmojiPackageMeta(
                        name = "demo-mets",
                        lang = "zh-TW",
                        metaDigest = "1f5a97dd6e6a3015c10f91b99338f9c6",
                        filesDigest = "7ee6fb9bf5ac1b8bc8c56c948e462873",
                        builds = listOf("debug","release","staging")
                    )
                ),
                trafficOrder = listOf(19,20,4,1,6,5),
                customOrder = listOf(12,13,11)
            )
        )

        Assertions.assertEquals(expected,actual,"Expected ${expected.toJson(gson)}, but was ${actual.toJson(gson)}")
        Assertions.assertEquals(
            setOf(
                "demo-mets-en-US" to 100000L,
                "demo-mets-ja-JP" to 100000L,
                "demo-mets-zh-TW" to 100000L,
                "demo-softbank-ja-JP" to 100001L),
            actual.getVersionSet())
    }

    @Test
    fun generate_lang_emoji_package_meta_with_update(){
        val src = File(srcPath)
        val old = setOf(
            LangEmojiPackageMeta(
                lang = "en-UK",
                packages = listOf(
                    EmojiPackageMeta(
                    name = "demo-mets",
                    lang = "en-UK"
                ).apply { versions = listOf(100000L) })
            ),
            LangEmojiPackageMeta(
                lang = "en-US",
                packages = listOf(
                    EmojiPackageMeta(
                    name = "demo-mets",
                    lang = "en-US"
                ).apply { versions = listOf(100003L) })
            ),
            LangEmojiPackageMeta(
                lang = "ja-JP",
                packages = listOf(
                    EmojiPackageMeta(
                    name = "demo-mets",
                    lang = "ja-JP"
                ).apply { versions = listOf(100005L) })
            ),
            LangEmojiPackageMeta(
                lang = "zh-TW",
                packages = listOf(
                    EmojiPackageMeta(
                    name = "demo-mets",
                    lang = "zh-TW"
                ).apply { versions = listOf(100010L) })
            )
        )

        val actual = Utility.generateLangEmojiPackageMeta(src, old)
        val expected = setOf(
            LangEmojiPackageMeta(
                lang = "en-UK",
                packages = listOf(
                    EmojiPackageMeta(
                        name = "demo-mets",
                        lang = "en-UK"
                    )
                )
            ),
            LangEmojiPackageMeta(
                lang = "en-US",
                packages = listOf(
                    EmojiPackageMeta(
                        name = "demo-mets",
                        lang = "en-US",
                        metaDigest = "0cb79cc089d48d1abffb2e10eac2ab79",
                        filesDigest = "7ee6fb9bf5ac1b8bc8c56c948e462873",
                        builds = listOf("debug","release","staging")
                    )
                ),
                trafficOrder = listOf(19,20,4,1,6,5),
                customOrder = listOf(12,13,11)
            ),
            LangEmojiPackageMeta(
                lang = "ja-JP",
                packages = listOf(
                    EmojiPackageMeta(
                        name = "demo-mets",
                        lang = "ja-JP",
                        metaDigest = "7be07d3fded6fe54c920b00a143ab3d1",
                        filesDigest = "7ee6fb9bf5ac1b8bc8c56c948e462873",
                        builds = listOf("debug","release","staging")
                    ),
                    EmojiPackageMeta(
                        name = "demo-softbank",
                        lang = "ja-JP",
                        metaDigest = "e542f8e3ad72cb5617efdf11188f3ad9",
                        filesDigest = "0cc4a79dfcb36a3fcf973c225d9db45c",
                        builds = listOf("debug","staging")
                    )
                ),
                trafficOrder = listOf(19,31,4,1,6,5),
                customOrder = listOf(33,13,11)
            ),
            LangEmojiPackageMeta(
                lang = "zh-TW",
                packages = listOf(
                    EmojiPackageMeta(
                        name = "demo-mets",
                        lang = "zh-TW",
                        metaDigest = "1f5a97dd6e6a3015c10f91b99338f9c6",
                        filesDigest = "7ee6fb9bf5ac1b8bc8c56c948e462873",
                        builds = listOf("debug","release","staging")
                    )
                ),
                trafficOrder = listOf(19,20,4,1,6,5),
                customOrder = listOf(12,13,11)
            )
        )

        Assertions.assertEquals(expected,actual,"Expected ${expected.toJson(gson)}, but was ${actual.toJson(gson)}")
        Assertions.assertEquals(
            setOf(
                "demo-mets-en-UK" to 100000L,
                "demo-mets-en-US" to 100004L,
                "demo-mets-ja-JP" to 100006L,
                "demo-softbank-ja-JP" to 100007L,
                "demo-mets-zh-TW" to 100011L),
            actual.getVersionSet())
    }

    @Test
    fun generate_lang_emoji_package_meta_with_update_more(){
        val src = File(srcPath)
        val old = setOf(
            LangEmojiPackageMeta(
                lang = "en-UK",
                packages = listOf(
                    EmojiPackageMeta(
                    name = "demo-mets",
                    lang = "en-UK"
                ).apply { versions = listOf(100000L) })
            ),
            LangEmojiPackageMeta(
                lang = "en-US",
                packages = listOf(
                    EmojiPackageMeta(
                    name = "demo-mets",
                    lang = "en-US"
                ).apply { versions = listOf(100003L) })
            ),
            LangEmojiPackageMeta(
                lang = "zh-TW",
                packages = listOf(
                    EmojiPackageMeta(
                    name = "demo-mets",
                    lang = "zh-TW"
                ).apply { versions = listOf(100010L) })
            )
        )

        val actual = Utility.generateLangEmojiPackageMeta(src, old)
        val expected = setOf(
            LangEmojiPackageMeta(
                lang = "en-UK",
                packages = listOf(
                    EmojiPackageMeta(
                        name = "demo-mets",
                        lang = "en-UK"
                    )
                )
            ),
            LangEmojiPackageMeta(
                lang = "en-US",
                packages = listOf(
                    EmojiPackageMeta(
                        name = "demo-mets",
                        lang = "en-US",
                        metaDigest = "0cb79cc089d48d1abffb2e10eac2ab79",
                        filesDigest = "7ee6fb9bf5ac1b8bc8c56c948e462873",
                        builds = listOf("debug","release","staging")
                    )
                ),
                trafficOrder = listOf(19,20,4,1,6,5),
                customOrder = listOf(12,13,11)
            ),
            LangEmojiPackageMeta(
                lang = "ja-JP",
                packages = listOf(
                    EmojiPackageMeta(
                        name = "demo-mets",
                        lang = "ja-JP",
                        metaDigest = "7be07d3fded6fe54c920b00a143ab3d1",
                        filesDigest = "7ee6fb9bf5ac1b8bc8c56c948e462873",
                        builds = listOf("debug","release","staging")
                    ),
                    EmojiPackageMeta(
                        name = "demo-softbank",
                        lang = "ja-JP",
                        metaDigest = "e542f8e3ad72cb5617efdf11188f3ad9",
                        filesDigest = "0cc4a79dfcb36a3fcf973c225d9db45c",
                        builds = listOf("debug","staging")
                    )
                ),
                trafficOrder = listOf(19,31,4,1,6,5),
                customOrder = listOf(33,13,11)
            ),
            LangEmojiPackageMeta(
                lang = "zh-TW",
                packages = listOf(
                    EmojiPackageMeta(
                        name = "demo-mets",
                        lang = "zh-TW",
                        metaDigest = "1f5a97dd6e6a3015c10f91b99338f9c6",
                        filesDigest = "7ee6fb9bf5ac1b8bc8c56c948e462873",
                        builds = listOf("debug","release","staging")
                    )
                ),
                trafficOrder = listOf(19,20,4,1,6,5),
                customOrder = listOf(12,13,11)
            )
        )

        Assertions.assertEquals(expected,actual,"Expected ${expected.toJson(gson)}, but was ${actual.toJson(gson)}")
        Assertions.assertEquals(
            setOf(
                "demo-mets-en-UK" to 100000L,
                "demo-mets-en-US" to 100004L,
                "demo-mets-ja-JP" to 100000L,
                "demo-softbank-ja-JP" to 100001L,
                "demo-mets-zh-TW" to 100011L),
            actual.getVersionSet())

    }

    @Test
    fun generate_lang_emoji_package_meta_with_update_new_lang(){
        val src = File(srcPath)
        val old = setOf(
            LangEmojiPackageMeta(
                lang = "en-UK",
                packages = listOf(
                    EmojiPackageMeta(
                    name = "demo-mets",
                    lang = "en-UK"
                ).apply { versions = listOf(100000L) })
            )
        )

        val actual = Utility.generateLangEmojiPackageMeta(src, old)
        val expected = setOf(
            LangEmojiPackageMeta(
                lang = "en-UK",
                packages = listOf(
                    EmojiPackageMeta(
                        name = "demo-mets",
                        lang = "en-UK"
                    )
                )
            ),
            LangEmojiPackageMeta(
                lang = "en-US",
                packages = listOf(
                    EmojiPackageMeta(
                        name = "demo-mets",
                        lang = "en-US",
                        metaDigest = "0cb79cc089d48d1abffb2e10eac2ab79",
                        filesDigest = "7ee6fb9bf5ac1b8bc8c56c948e462873",
                        builds = listOf("debug","release","staging")
                    )
                ),
                trafficOrder = listOf(19,20,4,1,6,5),
                customOrder = listOf(12,13,11)
            ),
            LangEmojiPackageMeta(
                lang = "ja-JP",
                packages = listOf(
                    EmojiPackageMeta(
                        name = "demo-mets",
                        lang = "ja-JP",
                        metaDigest = "7be07d3fded6fe54c920b00a143ab3d1",
                        filesDigest = "7ee6fb9bf5ac1b8bc8c56c948e462873",
                        builds = listOf("debug","release","staging")
                    ),
                    EmojiPackageMeta(
                        name = "demo-softbank",
                        lang = "ja-JP",
                        metaDigest = "e542f8e3ad72cb5617efdf11188f3ad9",
                        filesDigest = "0cc4a79dfcb36a3fcf973c225d9db45c",
                        builds = listOf("debug","staging")
                    )
                ),
                trafficOrder = listOf(19,31,4,1,6,5),
                customOrder = listOf(33,13,11)
            ),
            LangEmojiPackageMeta(
                lang = "zh-TW",
                packages = listOf(
                    EmojiPackageMeta(
                        name = "demo-mets",
                        lang = "zh-TW",
                        metaDigest = "1f5a97dd6e6a3015c10f91b99338f9c6",
                        filesDigest = "7ee6fb9bf5ac1b8bc8c56c948e462873",
                        builds = listOf("debug","release","staging")
                    )
                ),
                trafficOrder = listOf(19,20,4,1,6,5),
                customOrder = listOf(12,13,11)
            )
        )

        Assertions.assertEquals(expected,actual,"Expected ${expected.toJson(gson)}, but was ${actual.toJson(gson)}")
        Assertions.assertEquals(
            setOf(
                "demo-mets-en-UK" to 100000L,
                "demo-mets-en-US" to 100000L,
                "demo-mets-ja-JP" to 100000L,
                "demo-softbank-ja-JP" to 100001L,
                "demo-mets-zh-TW" to 100000L),
            actual.getVersionSet())

    }

    @Test
    fun generate_lang_emoji_package_meta_with_update_new_order(){
        val src = File(srcPath)
        val old = setOf(
            LangEmojiPackageMeta(
                lang = "en-US",
                packages = listOf(
                    EmojiPackageMeta(
                        name = "demo-mets",
                        lang = "en-US",
                        metaDigest = "0cb79cc089d48d1abffb2e10eac2ab79",
                        filesDigest = "7ee6fb9bf5ac1b8bc8c56c948e462873",
                        builds = listOf("debug","release","staging")
                    ).apply { versions = listOf(100000L) }
                ),
                trafficOrder = listOf(1,2,3,4,5,6,7,8,9),
                customOrder = listOf(11,12,13)
            )
        )

        val actual = Utility.generateLangEmojiPackageMeta(src, old)
        val expected = setOf(
            LangEmojiPackageMeta(
                lang = "en-US",
                packages = listOf(
                    EmojiPackageMeta(
                        name = "demo-mets",
                        lang = "en-US",
                        metaDigest = "0cb79cc089d48d1abffb2e10eac2ab79",
                        filesDigest = "7ee6fb9bf5ac1b8bc8c56c948e462873",
                        builds = listOf("debug","release","staging")
                    )
                ),
                trafficOrder = listOf(19,20,4,1,6,5),
                customOrder = listOf(12,13,11)
            ),
            LangEmojiPackageMeta(
                lang = "ja-JP",
                packages = listOf(
                    EmojiPackageMeta(
                        name = "demo-mets",
                        lang = "ja-JP",
                        metaDigest = "7be07d3fded6fe54c920b00a143ab3d1",
                        filesDigest = "7ee6fb9bf5ac1b8bc8c56c948e462873",
                        builds = listOf("debug","release","staging")
                    ),
                    EmojiPackageMeta(
                        name = "demo-softbank",
                        lang = "ja-JP",
                        metaDigest = "e542f8e3ad72cb5617efdf11188f3ad9",
                        filesDigest = "0cc4a79dfcb36a3fcf973c225d9db45c",
                        builds = listOf("debug","staging")
                    )
                ),
                trafficOrder = listOf(19,31,4,1,6,5),
                customOrder = listOf(33,13,11)
            ),
            LangEmojiPackageMeta(
                lang = "zh-TW",
                packages = listOf(
                    EmojiPackageMeta(
                        name = "demo-mets",
                        lang = "zh-TW",
                        metaDigest = "1f5a97dd6e6a3015c10f91b99338f9c6",
                        filesDigest = "7ee6fb9bf5ac1b8bc8c56c948e462873",
                        builds = listOf("debug","release","staging")
                    )
                ),
                trafficOrder = listOf(19,20,4,1,6,5),
                customOrder = listOf(12,13,11)
            )
        )

        Assertions.assertEquals(expected,actual,"Expected ${expected.toJson(gson)}, but was ${actual.toJson(gson)}")
        Assertions.assertEquals(
            setOf(
                "demo-mets-en-US" to 100000L,
                "demo-mets-ja-JP" to 100000L,
                "demo-softbank-ja-JP" to 100001L,
                "demo-mets-zh-TW" to 100000L),
            actual.getVersionSet())

    }

    @Test
    fun generate_lang_emoji_package_meta_with_new_lang_but_no_config(){
        val src = File(srcPath)
        //Drop en-UK,zh-TW,ja-JP
        val path = File(srcPath, EMOJI_FOLDER_NAME+File.separator+ LOCAL_LANG_CSV)
        val config = path.readLines().take(2)
        FileWriter(path).use { fw ->
            config.forEach { fw.write(it) }
        }
        Assertions.assertThrows(IllegalArgumentException::class.java){
            Utility.generateLangEmojiPackageMeta(src)
        }
    }
}

private fun Set<LangEmojiPackageMeta>.getVersionSet(): Set<Pair<String, Long>>{
    return this.flatMap { meta ->
        meta.packages.map { it.getNameWithLang() to it.getCurrentVersion() }
    }.toSet()
}

fun <E> Set<E>.toJson(gson: Gson): String {
    return gson.toJson(this)
}


