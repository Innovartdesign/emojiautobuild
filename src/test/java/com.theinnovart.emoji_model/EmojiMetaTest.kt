package com.theinnovart.emoji_model

import com.theinnovart.emoji_model.model.EmojiMeta
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

internal class EmojiMetaTest{

    @Test
    fun parse_csv_row_success(){
        val header = "id,version,icon_preview,still_image,category,publisher,icon_name,raw_name,langs,titles,hotwords"
        val sample =
            "1,2,,,traffic,innovart," +
                    "emoji_001_t_pedestrian_ahead_002," +
                    "emoji_001_t_pedestrian_ahead_002," +
                    "\"en-US,ja-JP,zh-TW\"," +
                    "\"Pedestrian\\nCrossing,歩行中,行人通行\"," +
                    "\"pedestrian ahead,前に歩行者がいます|歩行者|歩行中,行人通行\""

        val actual = EmojiMeta.parse(header, sample)
        val expected = EmojiMeta(
            1,
            2,
            "traffic",
            "innovart",
            "emoji_001_t_pedestrian_ahead_002.png",
            "emoji_001_t_pedestrian_ahead_002.emoji",
            listOf("en-US", "ja-JP", "zh-TW"),
            listOf("Pedestrian\\nCrossing", "歩行中", "行人通行"),
            listOf(
                listOf("pedestrian ahead"),
                listOf("前に歩行者がいます", "歩行者", "歩行中"),
                listOf("行人通行")
            )
        )

        Assertions.assertEquals(expected,actual)
    }

    @Test
    fun parse_csv_row_fail_due_to_langs_mismatch(){
        val header = "id,version,icon_preview,still_image,category,publisher,icon_name,raw_name,langs,titles,hotwords"
        val sample =
            "1,2,,,traffic,innovart," +
                    "emoji_001_t_pedestrian_ahead_002," +
                    "emoji_001_t_pedestrian_ahead_002," +
                    "\"en-US\"," +
                    "\"Pedestrian\\nCrossing,歩行中,行人通行\"," +
                    "\"pedestrian ahead,前に歩行者がいます|歩行者|歩行中,行人通行\""

        Assertions.assertThrows(RuntimeException::class.java) {
            EmojiMeta.parse(header, sample)
        }

    }

    @Test
    fun parse_csv_empty_row_fail(){
        val header = "id,version,icon_preview,still_image,category,publisher,icon_name,raw_name,langs,titles,hotwords"
        val sample =
            ",,,,,,,,,,"

        val actual = EmojiMeta.parse(header, sample)
        val expected = null

        Assertions.assertEquals(expected,actual)
    }

}