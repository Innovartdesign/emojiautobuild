package com.theinnovart.emoji_model

import mu.KotlinLogging
import org.jetbrains.annotations.TestOnly
import org.slf4j.Logger
import java.io.File

import java.lang.Exception
import java.nio.file.Files
import java.nio.file.Paths
import java.util.Date

object EmojiHexGenerator {

    @JvmStatic
    fun main(args: Array<String>) {
        export(args[0],args[1])
    }
    private val logger = KotlinLogging.logger {}

    @JvmStatic
    fun export(src: String, dst: String) {
        if(File(dst).exists()){
            File(dst).deleteRecursively()
        }
        File(dst).mkdirs();
        File(src)
                .listFiles { dir, name ->
                    name.endsWith(".emoji")
                }
                ?.map { Pair(it.name,it.readText()) }
                ?.map { convertSample(it.first,it.second) }
                ?.map { addPrefix(it.first,it.second) }
                ?.forEach { p ->
                    val name = p.first.replace(".emoji",".hex")
                    val bytes = p.second
                    File(dst,name).writeText(p.second)
                }

    }

    @TestOnly
    fun convertSample(name: String, text: String): Pair<String,ByteArray> {

        try {
            val t = Date().time
            return Pair(name, DevicePacketConverter.packUploadEmoji(text, t)!!)
        }catch (e:Exception){
            println(name)
            e.printStackTrace()
            throw (e)
        }
    }

    private fun addPrefix(name: String, payload: ByteArray): Pair<String, String> {
        val stringBuilder = StringBuilder()
        stringBuilder.append("char ").append(name.replace(".emoji", "")).append("[] = {").append("\n")
        var i = 0
        for (b in payload) {
            stringBuilder.append("0x").append(String.format("%02X", b))
            if (i != payload.size - 1)
                stringBuilder.append(", ")
            if (i % 8 == 0)
                stringBuilder.append("\n")
            i++
        }
        stringBuilder.append("};")
        return Pair(name,stringBuilder.toString())
    }


}
