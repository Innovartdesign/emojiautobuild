package com.theinnovart.emoji_model

import com.theinnovart.emoji_model.model.Manifest
import mu.KotlinLogging
import java.io.File
import java.io.FileReader
import java.io.FileWriter

class Exporter(val src: File, val dst: File, val intermediate: File){

    private lateinit var manifest: Manifest

    companion object {
        private val logger = KotlinLogging.logger {}

        @JvmStatic
        fun main(args: Array<String>) {
            Exporter(File(args[0]), File(args[1]), File(args[2]))
                .clear()
                .load()
                .update()
                .export()
        }

    }

    fun load(): Exporter {
        this.manifest = FileReader(File(src, GLOBAL_MANIFEST)).use { fr ->
            return@use Utility.getGson()
                .fromJson<Manifest>(fr, Manifest::class.java)
        }
        return this
    }

    fun getManifest(): Manifest {
        return manifest
    }

    fun export(): Exporter {
        
        Utility.buildEmojisPackages(src, intermediate, manifest)
        Utility.zipEmojiPackages(intermediate, dst)
        
        FileWriter(File(src, GLOBAL_MANIFEST)).use { fw ->
            fw.write(Utility.getGson().toJson(manifest))
        }

        FileWriter(File(dst, GLOBAL_MANIFEST)).use { fw ->
            fw.write(Utility.getGson().toJson(manifest))
        }
        return this
    }

    fun update(): Exporter {
        val updated =
            Utility.generateLangEmojiPackageMeta(src, manifest.emojis.toSet())
        
        manifest = manifest.copy(emojis = updated.toList().sortedBy { it.lang })

        return this
    }

    fun clear(): Exporter {
        dst.deleteRecursively()
        dst.mkdirs()
        intermediate.deleteRecursively()
        intermediate.mkdirs()
        return this
    }

}