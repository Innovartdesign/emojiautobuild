package com.theinnovart.emoji_model

import com.drew.imaging.ImageMetadataReader
import com.google.gson.GsonBuilder
import com.theinnovart.emoji_model.model.*
import mu.KotlinLogging
import java.io.*
import java.math.BigInteger
import java.security.MessageDigest
import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream

const val GLOBAL_MANIFEST = "emoji_package.json"
const val LOCAL_META_CSV = "list.csv"
const val LOCAL_BUILD_CSV = "build.csv"
const val LOCAL_LANG_CSV = "language.csv"
const val EXPORT_MANIFEST = "manifest.json"
const val ICON_PREFIX = "icon"
const val RAW_PREFIX = "raw"
const val EMOJI_FOLDER_NAME = "emojis"
val minPackageVersion: Map<String,Long> =
    mapOf("official" to 20L, "demo" to 100000L)

object Utility {
    private val logger = KotlinLogging.logger {}
    private val gson = GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create()!!

    fun getGson() = gson

    fun generateEmojiPackageMeta(root: File) : Set<EmojiPackageMeta>{

        if(!root.exists()){
            logger.warn { "root is not a exists!" }
            return emptySet()
        }

        if(root.isFile){
            logger.warn { "root is not a dir!" }
            return emptySet()
        }

        val emojis = parseEmojiMetaCSV(root) ?: return kotlin.run{
            logger.warn { "${root.path}: $LOCAL_META_CSV missing" }
            emptySet()
        }

        val builds = parseBuildCSV(root) ?: return kotlin.run{
            logger.warn { "${root.path}: $LOCAL_BUILD_CSV missing" }
            emptySet()
        }

        //get all languages from emojis metadata
        val languages = emojis
            .flatMap { it.languages }
            .distinct()

//        logger.debug { languages }

        return languages.map {
            val (metaDigest, fileDigest) = generateEmojiPackageDigest(root, emojis, it)

            EmojiPackageMeta(
                name = root.name,
                lang = it,
                filesDigest = fileDigest,
                metaDigest = metaDigest,
                builds = builds
            )
        }.toSet()

    }

    private fun parseLanguageCSV(root: File): List<LanguageConfig>? {
        val metaCSV = File(root, EMOJI_FOLDER_NAME).listFiles()
            ?.find { it.name == LOCAL_LANG_CSV && it.isFile && !it.isHidden }
            ?:return null

        val languages = metaCSV.useLines { seq ->
            val list = seq.toList()
            val header  = list.first()
            return@useLines list
                .drop(1)
                .filter { !it.startsWith("#") }
                .mapNotNull {
                    LanguageConfig.parse(header,it)
                }
                .toList()
        }

        return languages
    }

    private fun parseBuildCSV(root: File): List<String>? {
        val buildCSV = root.listFiles()
            ?.find { it.name == LOCAL_BUILD_CSV && it.isFile && !it.isHidden }
            ?:return null

        val builds = buildCSV.useLines { seq ->
            val regex = ",(?=([^\"]*\"[^\"]*\")*[^\"]*$)".toRegex()

            return@useLines seq
                .firstOrNull()
                ?.split(regex)
                ?.filter { it.isNotBlank() }
                ?.sortedBy { it }
                ?.toList() ?: emptyList()
        }

        return builds
    }

    private fun parseEmojiMetaCSV(root: File): List<EmojiMeta>? {
        val metaCSV = root.listFiles()
            ?.find { it.name == LOCAL_META_CSV && it.isFile && !it.isHidden }
            ?:return null

        val emojis = metaCSV.useLines{ seq ->
            val list = seq.toList()
            val header  = list.first()
            return@useLines list
                .drop(1)
                .filter { !it.startsWith("#") }
                .mapNotNull {
                    EmojiMeta.parse(header,it)
                }
                .sortedBy { it.id }
                .toList()
        }

        return emojis
    }

    private fun generateEmojiPackageDigest(root: File, emojisMetadataList: List<EmojiMeta>, lang: String): Pair<String, String> {
        val metadataDigest = emojisMetadataList
            //Only consider emoji with language
            .filter { it.languages.contains(lang) }
            .map { emoji ->
                val idx = emoji.languages.indexOf(lang)
                return@map listOf(
                        emoji.id,
                        emoji.version,
                        emoji.category,
                        emoji.publisher,
                        emoji.languages[idx],
                        emoji.titles[idx],
                        *(emoji.hotWords[idx].sorted().toTypedArray())
                    )
                    .joinToString(".")
//                    .also { logger.debug { it } }

            }
            .reduce { acc, digest -> acc + digest }
            .md5()

        val assetsDigest = emojisMetadataList
            .map { emoji ->
                val icon = File(root, ICON_PREFIX+File.separatorChar+emoji.iconName)
                val raw = File(root, RAW_PREFIX+File.separatorChar+emoji.rawName)
                checkRawEmojiFormat(raw,emoji)
                checkIconFormat(icon)
                return@map raw.readText().md5()+icon.readText().md5()
            }
            .reduce { acc, digest -> acc + digest }
            .md5()

        return metadataDigest to assetsDigest
    }

    private fun checkIconFormat(icon: File) {
        if (!icon.exists() || !icon.isFile) {
            throw Throwable("Emoji icon檔找不到: ${icon.path}")
        }
        //Throw exception if corrupt
        try {
            ImageMetadataReader.readMetadata(icon)
        }catch (e: Exception){
            throw Throwable("Emoji icon檔案格式錯誤: ${icon.path}").apply { addSuppressed(e) }
        }
    }

    fun copyAsset(src: File, dst: File) {
        src.copyRecursively(dst,true)
    }

    fun generateLangEmojiPackageMeta(src: File, oldSet: Set<LangEmojiPackageMeta> = emptySet()): Set<LangEmojiPackageMeta> {

        val emojiSrc = File(src, EMOJI_FOLDER_NAME)
        val metas =
            emojiSrc.listFiles()
                ?.filter { it.isDirectory && !it.isHidden }
                ?.flatMap { generateEmojiPackageMeta(it) }
                ?:return emptySet()

        val langConfigs = parseLanguageCSV(src) ?: return kotlin.run {
            logger.warn { "${src.path}: $EMOJI_FOLDER_NAME/$LOCAL_LANG_CSV 找不到" }
            emptySet()
        }

        val newSet = metas
            .groupBy { it.lang }
            .map { (lang, list) ->
                //If not found cofig
                val langConfig = langConfigs.find { it.lang == lang }
                    ?:throw IllegalArgumentException("找不到 $lang 的設定檔，請至language.csv新增")

                LangEmojiPackageMeta(
                    lang = lang,
                    packages = list,
                    trafficOrder = langConfig.trafficOrder,
                    customOrder = langConfig.customOrder
                )
            }
            .toSet()

        val res = oldSet.toMutableSet()

        newSet.forEach { new ->
            val old = oldSet.find { old -> old.lang == new.lang }

            if(old == null) {
                val base = new.packages.toMutableList()

                new.packages.sortedBy { it.name }.forEach { meta ->
                    meta.versions = meta.versions + getNextPackageVersion(meta, new.packages)
                }

                res.add(new.copy(packages = base.sortedBy { it.name }))
            }else{
                res.remove(old)
                //newest will be last.
                val base = old.packages.toMutableList()
                new.packages.sortedBy { it.name }.forEach { meta ->
                    val prev = base.find { it.name == meta.name }

                    if(prev !=null &&
                        prev.filesDigest == meta.filesDigest &&
                        prev.metaDigest == meta.metaDigest &&
                            prev.builds == meta.builds){
                        logger.warn { "${meta.getNameWithLang()} Package未變動" }

                    } else if(prev != null){
                        base.add(meta.copy().apply {
                            versions = (prev.versions + getNextPackageVersion(meta, base)).sorted()
                            logger.warn { "${meta.getNameWithLang()} Package更新 ${prev.versions.last()} -> ${versions.last()}" }
                        })
                        base.remove(prev)
                    } else {
                        base.add(meta.copy().apply {
                            versions = versions + getNextPackageVersion(meta, base)
                            logger.warn { "${meta.getNameWithLang()} Package增加 ${versions.last()}" }

                        })

                    }

                }
                val trafficOrder =  new.trafficOrder.takeIf { it.isNotEmpty() } ?:old.trafficOrder
                val customOrder =  new.customOrder.takeIf { it.isNotEmpty() } ?:old.customOrder

                res.add(new.copy(
                    packages = base.sortedBy { it.name },
                    trafficOrder = trafficOrder,
                    customOrder = customOrder
                ))
            }
        }

        return res
    }

    //FIXME. If official version get too large might overlay demo/others version.
    fun getNextPackageVersion(
        meta: EmojiPackageMeta,
        packages: List<EmojiPackageMeta>
    ): Long {
        val prefix = meta.name.split("-").first()
        val filteredPackages = packages
            .filter { it.name.startsWith(prefix) }
        return filteredPackages
            .flatMap { it.versions }
            .max()
            ?.plus(1)
            ?: minPackageVersion.getOrDefault(prefix,200000L)
    }

    private fun checkRawEmojiFormat(
        src: File,
        emoji: EmojiMeta
    ) {

        if (!src.exists() || !src.isFile) {
            throw Throwable("Emoji raw檔找不到: ${emoji.rawName}")
        }

        val raw: RawEmoji = FileReader(src).use { reader ->
            gson.fromJson(reader, RawEmoji::class.java)
        }

        if (raw.id != emoji.id) {
            throw Throwable("Emoji id 不符合: ${emoji.rawName}")
        }

        if (raw.subversion != emoji.version) {
            throw Throwable("Emoji version 不符合 : ${emoji.rawName}")
        }

        raw.colorPalette.forEach { color ->
            if (!DeviceColorMap.containsColor(color)) {
                throw Throwable("${emoji.rawName} color missing $color")
            }
        }

    }

    fun buildEmojisPackages(src: File, dst: File, manifest: Manifest) {
        val emojiSrc = File(src, EMOJI_FOLDER_NAME)
        manifest.emojis
            .flatMap { it.packages }
            .forEach { meta ->

                val emojis = parseEmojiMetaCSV(File(emojiSrc,meta.name))

                if(emojis.isNullOrEmpty()){
                    logger.debug { "${emojiSrc.path}: is not target dir" }
                    return@forEach
                }

                val filteredEmojis = emojis
                    .filter { it.languages.contains(meta.lang) }
                    .sortedBy { it.id }

                if(filteredEmojis.isNullOrEmpty()) {
                    logger.debug { "${File(emojiSrc,meta.name).path}: no ${meta.lang} emojis belong to this dir" }
                    return@forEach
                }

                val dir = File(dst,meta.getExportDirName())
                dir.mkdirs()

                filteredEmojis
                    .forEach {
                        val assetEmojiSrc = File(emojiSrc, listOf(meta.name, RAW_PREFIX, it.rawName).joinToString(File.separator))
                        val assetEmojiDst = File(dst, listOf(meta.getExportDirName(), RAW_PREFIX, it.rawName).joinToString(File.separator))
                        assetEmojiSrc.copyTo(assetEmojiDst, true)

                        val assetIconSrc = File(emojiSrc, listOf(meta.name, ICON_PREFIX, it.iconName).joinToString(File.separator))
                        val assetIconDst = File(dst, listOf(meta.getExportDirName(), ICON_PREFIX, it.iconName).joinToString(File.separator))
                        assetIconSrc.copyTo(assetIconDst, true)
                    }

                val exportedEmojis = filteredEmojis.map {
                    EmojiPackageManifest.ExportEmoji(
                        id = it.id,
                        version = it.version,
                        category = it.category,
                        publisher = it.publisher,
                        iconName = it.iconName,
                        rawName = it.rawName,
                        title = it.getTitle(meta.lang),
                        hotWordList = it.getHotWords(meta.lang)
                    )
                }
                val manifestContent = EmojiPackageManifest(
                    version = meta.getCurrentVersion(),
                    langCode = meta.lang,
                    emojis = exportedEmojis
                )

                FileWriter(File(dir, EXPORT_MANIFEST)).use {
                    it.write(manifestContent.toBackslashEscapedJson())
                }

            }
    }

    fun zipEmojiPackages(src: File, dst: File) {
            src.listFiles(FileFilter { it.isDirectory && !it.isHidden })
            ?.forEach { folder ->
                val zipFilePath = listOf(dst, folder.name.toLowerCase() + ".zip").joinToString(File.separator)
                ZipOutputStream(FileOutputStream(zipFilePath)).use { zos ->
                    zipFile(true, folder, folder.name, zos)
                }
            }
    }

    private fun zipFile(
        isRoot: Boolean,
        fileToZip: File,
        fileName: String,
        zipOut: ZipOutputStream
    ) {
        if (fileToZip.isHidden) {
            return
        }

        if (fileToZip.isDirectory) {

            if (!isRoot) {
                if (fileName.endsWith("/")) {
                    zipOut.putNextEntry(ZipEntry(fileName))
                    zipOut.closeEntry()
                } else {
                    zipOut.putNextEntry(ZipEntry("$fileName/"))
                    zipOut.closeEntry()
                }
            }

            val children = fileToZip.listFiles()
            for (childFile in children!!) {
                zipFile(
                    false,
                    childFile,
                    (if (isRoot) "" else fileName) + "/" + childFile.name,
                    zipOut
                )
            }
            return
        }

        FileInputStream(fileToZip).use { fis ->
            val zipEntry = ZipEntry(fileName)
            zipOut.putNextEntry(zipEntry)
            zipOut.write(fis.readBytes())
        }
    }

}

fun String.md5(): String {
    val md = MessageDigest.getInstance("MD5")
    return BigInteger(1, md.digest(toByteArray())).toString(16).padStart(32, '0')
}

