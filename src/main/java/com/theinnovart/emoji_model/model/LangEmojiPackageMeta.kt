package com.theinnovart.emoji_model.model


import com.google.gson.annotations.SerializedName

data class Manifest(
    @SerializedName("version_name")
    val versionName: String = "v0.0.00",

    @SerializedName("emojis")
    val emojis: List<LangEmojiPackageMeta> = listOf()
)

data class LanguageConfig(
    @SerializedName("lang")
    val lang: String = "",
    @SerializedName("traffic_order")
    val trafficOrder: List<Long> = listOf(),
    @SerializedName("custom_order")
    val customOrder: List<Long> = listOf()
){
    companion object{
        //Need consider comma inside column
        //https://stackoverflow.com/questions/53997728/parse-csv-to-kotlin-list
        private val regex = ",(?=([^\"]*\"[^\"]*\")*[^\"]*$)".toRegex()

        fun parse(header: String,rawStr: String): LanguageConfig? {
            val headers = header
                .split(regex)
                .mapIndexed { index, s -> s.trim() to index }
                .toMap()
            val attrs = rawStr.split(regex).map { it.trim() }
            val language = attrs[headers["language"] ?: error("language missing")].trim() //traffic, expression
            val traffic_order = attrs[headers["traffic_order"] ?: error("traffic_order missing")]
                .trim()
                .trim('\"')
                .split(",")
                .map { it.toLong() }
            val custom_order = attrs[headers["custom_order"] ?: error("custom_order missing")]
                .trim()
                .trim('\"')
                .split(",")
                .map { it.toLong() }

            if(language.isBlank()) {
                return null
            }

            return LanguageConfig(
                lang = language,
                trafficOrder = traffic_order,
                customOrder = custom_order
            )
        }
    }
}

data class LangEmojiPackageMeta(
    @SerializedName("lang")
    val lang: String = "",
    @SerializedName("packages")
    val packages: List<EmojiPackageMeta> = listOf(),
    @SerializedName("traffic_order")
    val trafficOrder: List<Long> = listOf(),
    @SerializedName("custom_order")
    val customOrder: List<Long> = listOf()
)

data class EmojiPackageMeta(

    @SerializedName("name")
    val name: String  = "",
    @SerializedName("lang")
    val lang: String = "",
    @SerializedName("meta_digest")
    val metaDigest: String = "",
    @SerializedName("files_digest")
    val filesDigest: String = "",
    @SerializedName("builds")
    val builds: List<String> = listOf()
) {
    fun getNameWithLang(): String {
        return listOf(name,lang).joinToString("-")
    }

    fun getExportDirName(): String {
        return listOf(name,lang,getCurrentVersion()).joinToString("-")
    }

    fun getCurrentVersion(): Long {
        if(versions.isEmpty()) throw RuntimeException("versions empty")
        return versions.last()
    }

    @SerializedName("versions")
    var versions: List<Long> = emptyList()
}