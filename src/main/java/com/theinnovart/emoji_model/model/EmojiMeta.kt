package com.theinnovart.emoji_model.model

import com.google.gson.GsonBuilder
import com.google.gson.annotations.SerializedName
import java.lang.RuntimeException

data class EmojiPackageManifest(

    @SerializedName("e_version")
    val version: Long = 0,

    @SerializedName("platform")
    val platform: String = "android",

    @SerializedName("language_code")
    val langCode: String = "",

    @SerializedName("emojis")
    var emojis: List<ExportEmoji> = listOf()

) {
    data class ExportEmoji(
        @SerializedName("id")
        val id: Long = 0,

        @SerializedName("version")
        val version: Int = 0,

        @SerializedName("publisher")
        val publisher: String = "",

        @SerializedName("category")
        val category: String = "",

        @SerializedName("title")
        val title: String = "",

        @SerializedName("hotword")
        val hotWordList: List<String> = listOf(),

        @SerializedName("icon_name")
        val iconName: String = "",

        @SerializedName("raw_name")
        val rawName: String = ""
    )

    companion object{
        val gson = GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create()
    }

    fun toBackslashEscapedJson() = gson.toJson(this, EmojiPackageManifest::class.java).replace("\\\\","\\")
}


data class EmojiMeta(

    @SerializedName("id")
    val id: Long = 0,

    @SerializedName("version")
    val version: Int = 0,

    @SerializedName("category")
    val category: String = "",

    @SerializedName("publisher")
    val publisher: String = "",

    @SerializedName("icon_name")
    val iconName: String = "",

    @SerializedName("raw_name")
    val rawName: String = "",

    @SerializedName("langs")
    val languages: List<String> = listOf(),

    @SerializedName("titles")
    val titles: List<String> = listOf(),

    @SerializedName("hotwords")
    val hotWords: List<List<String>> = listOf()
){
    fun getTitle(lang: String): String {
        return titles[languages.indexOf(lang)]
    }

    fun getHotWords(lang: String): List<String> {
        return hotWords[languages.indexOf(lang)]
    }

    companion object {
        //Need consider comma inside column
        //https://stackoverflow.com/questions/53997728/parse-csv-to-kotlin-list
        private val regex = ",(?=([^\"]*\"[^\"]*\")*[^\"]*$)".toRegex()

        fun parse(header: String,rawStr: String): EmojiMeta? {
            val headers = header
                .split(regex)
                .mapIndexed { index, s -> s.trim() to index }
                .toMap()
            val attrs = rawStr.split(regex).map { it.trim() }
            val id = attrs[headers["id"] ?: error("id missing")].toLongOrNull() ?: return null
            val ver = attrs[headers["version"] ?: error("version missing")].toInt()
            val category = attrs[headers["category"] ?: error("category missing")].toLowerCase().trim() //traffic, expression
            val publisher = attrs[headers["publisher"] ?: error("publisher missing")].toLowerCase().trim()
            val iconName = attrs[headers["icon_name"] ?: error("icon_name missing")].toLowerCase().trim().plus(".png")
            val rawName = attrs[headers["raw_name"] ?: error("raw_name missing")].toLowerCase().trim().plus(".emoji")
            val langs = attrs[headers["langs"] ?: error("langs missing")].trim().trim('\"').split(",")
            val titles = attrs[headers["titles"] ?: error("titles missing")].trim().trim('\"').split(",")
            val hotWords = attrs[headers["hotwords"] ?: error("hotwords missing")]
                .trim().trim('\"').split(",")
                .map {
                    it.split("|").map { term ->
                        term.trim()
                    }
                }

            if(langs.isEmpty()) {
                throw RuntimeException("未指定任何Language")
            }

            if(langs.size != titles.size) {
                throw RuntimeException("Language數量與標題數量不符")
            }

            if(langs.size != hotWords.size) {
                throw RuntimeException("Language數量與喚醒詞數量不符")
            }

            return EmojiMeta(
                id = id,
                version = ver,
                category = category,
                publisher = publisher,
                iconName = iconName,
                rawName = rawName,
                languages = langs,
                titles = titles,
                hotWords = hotWords
            )
        }
    }
}

