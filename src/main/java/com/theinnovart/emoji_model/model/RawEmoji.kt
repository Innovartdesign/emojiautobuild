package com.theinnovart.emoji_model.model

import com.google.gson.annotations.SerializedName

data class RawEmoji(

    @SerializedName("name")
    val name: String = "",

    @SerializedName("description")
    val description: String = "",

    @SerializedName("id")
    val id: Long = 0,

    @SerializedName("subversion")
    val subversion: Int = 0,

    @SerializedName("pages")
    val pages: Int = 0,

    @SerializedName("monostatic")
    val monostatic: Int = 0,

    @SerializedName("colorstatic")
    val colorstatic: Int = 0,

    @SerializedName("color_palette")
    val colorPalette: List<String> = listOf(),

    @SerializedName("mono_palette")
    val monoPalette: List<String> = listOf(),

    @SerializedName("period")
    val periods: List<Int> = listOf(),

    @SerializedName("layout")
    val layout: List<List<Dot>> = listOf()
)


data class Dot(

    @SerializedName("palette_idx")
    val paletteIdx: Int = 0,

    @SerializedName("idx")
    val idx: Int = 0
)