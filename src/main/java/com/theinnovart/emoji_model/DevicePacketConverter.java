package com.theinnovart.emoji_model;

import com.theinnovart.emoji_model.model.RawEmoji;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DevicePacketConverter {

    private static final byte FORMAT_VERSION = 0x01;
    private static final byte RESERVED = 0x00;

    public static class DeviceAdvertisingRecord {
        public final String serial;
        public final long timestamp;
        public DeviceAdvertisingRecord(String serial, long timestamp){
            this.serial = serial;
            this.timestamp = timestamp;
        }
    }

    public static RawEmoji toEmoji(String json){
        return Utility.INSTANCE.getGson().fromJson(json, RawEmoji.class);
    }

    public static byte[] packUploadEmoji(String rawEmojiJson, long timestamp){
        return internalPackUploadEmoji(toEmoji(rawEmojiJson), timestamp);
    }

    private static byte[] internalPackUploadEmoji(RawEmoji emoji, long timestamp){
        int idx = 2; // First 2 byte is total length

        ByteBuffer byteBuffer = ByteBuffer.allocate(65536).order(ByteOrder.LITTLE_ENDIAN);

        byteBuffer.put(idx,FORMAT_VERSION);
        idx+=1;

        byteBuffer.putInt(idx,(int) emoji.getId());
        idx+=4;

        byteBuffer.put(idx, (byte) emoji.getSubversion());
        idx+=1;

        //LED count
        byteBuffer.putShort(idx, (short) emoji.getLayout().get(0).size());
        idx+=2;

        byteBuffer.put(idx, (byte) emoji.getColorstatic());
        idx+=1;

        byteBuffer.put(idx, (byte) emoji.getMonostatic());
        idx+=1;

        byteBuffer.put(idx, (byte) emoji.getPages());
        idx+=1;

        byteBuffer.putLong(idx,timestamp);
        idx+=8;

        //Todo, don't forget check emoji format.
        if(emoji.getColorPalette() == null || emoji.getColorPalette().size() != 16){
            throw new RuntimeException("should be 16 terms");
        }
        for(int i=0;i<emoji.getColorPalette().size();i++){
            String color = emoji.getColorPalette().get(i);
            byteBuffer.put(idx,DeviceColorMap.INSTANCE.getColorIndex(color));
//            System.out.println(color.toUpperCase()+" -> "+ byteToHexString(fullColorMap.get(color.toUpperCase()).byteValue()));
            idx+=1;
        }

        if(emoji.getMonoPalette() == null || emoji.getMonoPalette().size() != 16){
            throw new RuntimeException("should be 16 terms");
        }
        for(int i=0;i<emoji.getColorPalette().size();i++){
            String color = emoji.getMonoPalette().get(i);
            byteBuffer.put(idx,DeviceColorMap.INSTANCE.getColorIndex(color));
            idx+=1;
        }

        for(int i=0;i<emoji.getPages();i++){

            byteBuffer.put(idx, (byte)(emoji.getPeriods().get(i)/100));
            idx+=1;

            for(int j=0;j<emoji.getLayout().get(i).size();j+=2){
                int first = emoji.getLayout().get(i).get(j).getPaletteIdx();
                int second = ((j+1) < emoji.getLayout().get(i).size()) ?
                        emoji.getLayout().get(i).get(j+1).getPaletteIdx() : 0;
                byteBuffer.put(idx, mergeToByte(first,second));
                idx+=1;
            }

        }

        byteBuffer.putShort(0, (short) idx);

        return Arrays.copyOfRange(byteBuffer.array(),0,idx);

    }

    public static byte[] packDisplayEmoji(long id, boolean isMono, boolean isStatic, int brightness, long timestamp){
        return internalPackPlayEmoji(id,isMono,isStatic,brightness,timestamp);
    }

    private static byte[] internalPackPlayEmoji(long id, boolean isMono, boolean isStatic, int brightness, long timestamp){
        int idx = 2; // First 2 byte is total length

        ByteBuffer byteBuffer = ByteBuffer.allocate(19).order(ByteOrder.LITTLE_ENDIAN);

        byteBuffer.put(idx, FORMAT_VERSION);
        idx+=1;

        byteBuffer.put(idx, RESERVED);
        idx+=1;

        byteBuffer.putInt(idx, (int) id);
        idx+=4;

        //Color = 0x00, mono = 0x01
        byteBuffer.put(idx, (byte) (isMono?0x01:0x00));
        idx+=1;

        //Animation = 0x00, static = 0x01
        byteBuffer.put(idx, (byte) (isStatic?0x01:0x00));
        idx+=1;

        // 0 ~ 255
        byteBuffer.put(idx, (byte) brightness);
        idx+=1;

        byteBuffer.putLong(idx,timestamp);
        idx+=8;

        byteBuffer.putShort(0, (short) idx);

        return Arrays.copyOfRange(byteBuffer.array(),0,idx);

    }

    public static byte[] packDeleteEmoji(long id, long timestamp){
        return internalPackDeleteEmoji(id,timestamp);
    }

    private static byte[] internalPackDeleteEmoji(long id, long timestamp){
        int idx = 2; // First 2 byte is total length

        ByteBuffer byteBuffer = ByteBuffer.allocate(65536).order(ByteOrder.LITTLE_ENDIAN);

        byteBuffer.put(idx, FORMAT_VERSION);
        idx+=1;

        byteBuffer.put(idx, RESERVED);
        idx+=1;

        byteBuffer.put(idx, RESERVED);
        idx+=1;

        byteBuffer.put(idx, RESERVED);
        idx+=1;

        byteBuffer.putInt(idx, (int) id);
        idx+=4;

        byteBuffer.putLong(idx,timestamp);
        idx+=8;

        byteBuffer.putShort(0, (short) idx);

        return Arrays.copyOfRange(byteBuffer.array(),0,idx);

    }


    public static byte[] packDeviceTimestamp(long timestamp) {
        return internalPackDeviceTimestamp(timestamp);
    }

    private static byte[] internalPackDeviceTimestamp(long timestamp){
        int idx = 2; // First 2 byte is total length

        ByteBuffer byteBuffer = ByteBuffer.allocate(14).order(ByteOrder.LITTLE_ENDIAN);

        byteBuffer.put(idx, FORMAT_VERSION);
        idx+=1;

        byteBuffer.put(idx, RESERVED);
        idx+=1;

        byteBuffer.put(idx, RESERVED);
        idx+=1;

        byteBuffer.put(idx, RESERVED);
        idx+=1;

        byteBuffer.putLong(idx,timestamp);
        idx+=8;

        byteBuffer.putShort(0, (short) idx);

        return Arrays.copyOfRange(byteBuffer.array(),0,idx);

    }

    public static List<byte[]> pack(byte[] raw, int mtu) {
        if(mtu <= 0 || raw.length<=0){
            throw new IllegalArgumentException("mtu,data length must be positive");
        }
        int n = raw.length / (mtu - 1 );

        if(raw.length % (mtu -1) != 0) n++;

        ArrayList<byte[]> list = new ArrayList<>(n);

        byte[] tmp;

        int m = 1;

        for(int i = 0 ; i<raw.length; i+=(mtu-1)){
            tmp = new byte[m==n?raw.length-i+1:mtu];

            tmp[0] = (byte) (m==n? 0x00:0x01);
            System.arraycopy(raw,i,tmp,1,(m==n?raw.length-i:mtu-1));
            list.add(tmp);
            m++;
        }

        return list;

    }

    public static ByteBuffer unpack(byte[] value, boolean isFirstPack) {
        if(isFirstPack){
            //Ignore header(1byte)
            ByteBuffer buf = ByteBuffer.wrap(value,1,2)
                    .order(ByteOrder.LITTLE_ENDIAN);

            //entire message length
            Short len = buf.getShort();

            if(len<2){
                throw new RuntimeException("len must greater than 2");
            }

            //Allocate entire message length except header(1byte) and length itself(2bytes)
            return ByteBuffer
                    .allocate(len-2)
                    .order(ByteOrder.LITTLE_ENDIAN)
                    .put(value,3,value.length - 3);
        }else {
            //Exclude header(1byte)
            return ByteBuffer
                    .wrap(value,1,value.length-1)
                    .order(ByteOrder.LITTLE_ENDIAN);
        }
    }

    public static boolean isLastPack(byte[] value) {
        if(value == null || value.length == 0){
            throw new RuntimeException("value is empty || null");
        }
        return ((value[0] & 0xFF) == 0x00);
    }


    static byte mergeToByte(int first4Bits, int second4Bits){
        if(first4Bits< 0 || first4Bits>15){
            throw new IllegalArgumentException("must >=0 && <=15");
        }
        if(second4Bits< 0 || second4Bits>15){
            throw new IllegalArgumentException("must >=0 && <=15");
        }
        return (byte) ((first4Bits & 0x0F) << 4 | (second4Bits & 0x0F));
    }

    public static byte queryPaletteIndex(String color) {
        return (byte)( (DeviceColorMap.INSTANCE.getColorIndex(color) & 0x000000ff) );
    }

    public static long toUnsignedInt(int i) {
        return i&0x0FFFFFFFFL;
    }

    public static int toUnsignedShort(short s) {
        return s&0x0FFFF;
    }

    public static int toUnsignedByte(byte b) {
        return b&0x0FF;
    }


}
