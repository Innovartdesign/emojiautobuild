package com.theinnovart.emoji_model

object DeviceColorMap {

    fun getColorIndex(colorHex: String): Byte {
        return fullColorMap[colorHex.toUpperCase()]!!.toByte()
    }

    fun containsColor(colorHex: String): Boolean {
        return fullColorMap.containsKey(colorHex.toUpperCase())
    }

    val fullColorMap = HashMap<String, Int>()

    //Color palette based on v0.18 spec.
    init {
        fullColorMap.put("#000000", 0);
        fullColorMap.put("#00003F", 1);
        fullColorMap.put("#00006F", 2);
        fullColorMap.put("#00009F", 3);
        fullColorMap.put("#0000CF", 4);
        fullColorMap.put("#0000FF", 5);
        fullColorMap.put("#003F00", 6);
        fullColorMap.put("#003F3F", 7);
        fullColorMap.put("#003F6F", 8);
        fullColorMap.put("#003F9F", 9);
        fullColorMap.put("#003FCF", 10);
        fullColorMap.put("#003FFF", 11);
        fullColorMap.put("#006F00", 12);
        fullColorMap.put("#006F3F", 13);
        fullColorMap.put("#006F6F", 14);
        fullColorMap.put("#006F9F", 15);
        fullColorMap.put("#006FCF", 16);
        fullColorMap.put("#006FFF", 17);
        fullColorMap.put("#009F00", 18);
        fullColorMap.put("#009F3F", 19);
        fullColorMap.put("#009F6F", 20);
        fullColorMap.put("#009F9F", 21);
        fullColorMap.put("#009FCF", 22);
        fullColorMap.put("#009FFF", 23);
        fullColorMap.put("#00CF00", 24);
        fullColorMap.put("#00CF3F", 25);
        fullColorMap.put("#00CF6F", 26);
        fullColorMap.put("#00CF9F", 27);
        fullColorMap.put("#00CFCF", 28);
        fullColorMap.put("#00CFFF", 29);
        fullColorMap.put("#00FF00", 30);
        fullColorMap.put("#00FF3F", 31);
        fullColorMap.put("#00FF6F", 32);
        fullColorMap.put("#00FF9F", 33);
        fullColorMap.put("#00FFCF", 34);
        fullColorMap.put("#00FFFF", 35);
        fullColorMap.put("#3F0000", 36);
        fullColorMap.put("#3F003F", 37);
        fullColorMap.put("#3F006F", 38);
        fullColorMap.put("#3F009F", 39);
        fullColorMap.put("#3F00CF", 40);
        fullColorMap.put("#3F00FF", 41);
        fullColorMap.put("#3F3F00", 42);
        fullColorMap.put("#3F3F3F", 43);
        fullColorMap.put("#3F3F6F", 44);
        fullColorMap.put("#3F3F9F", 45);
        fullColorMap.put("#3F3FCF", 46);
        fullColorMap.put("#3F3FFF", 47);
        fullColorMap.put("#3F6F00", 48);
        fullColorMap.put("#3F6F3F", 49);
        fullColorMap.put("#3F6F6F", 50);
        fullColorMap.put("#3F6F9F", 51);
        fullColorMap.put("#3F6FCF", 52);
        fullColorMap.put("#3F6FFF", 53);
        fullColorMap.put("#3F9F00", 54);
        fullColorMap.put("#3F9F3F", 55);
        fullColorMap.put("#3F9F6F", 56);
        fullColorMap.put("#3F9F9F", 57);
        fullColorMap.put("#3F9FCF", 58);
        fullColorMap.put("#3F9FFF", 59);
        fullColorMap.put("#3FCF00", 60);
        fullColorMap.put("#3FCF3F", 61);
        fullColorMap.put("#3FCF6F", 62);
        fullColorMap.put("#3FCF9F", 63);
        fullColorMap.put("#3FCFCF", 64);
        fullColorMap.put("#3FCFFF", 65);
        fullColorMap.put("#3FFF00", 66);
        fullColorMap.put("#3FFF3F", 67);
        fullColorMap.put("#3FFF6F", 68);
        fullColorMap.put("#3FFF9F", 69);
        fullColorMap.put("#3FFFCF", 70);
        fullColorMap.put("#3FFFFF", 71);
        fullColorMap.put("#6F0000", 72);
        fullColorMap.put("#6F003F", 73);
        fullColorMap.put("#6F006F", 74);
        fullColorMap.put("#6F009F", 75);
        fullColorMap.put("#6F00CF", 76);
        fullColorMap.put("#6F00FF", 77);
        fullColorMap.put("#6F3F00", 78);
        fullColorMap.put("#6F3F3F", 79);
        fullColorMap.put("#6F3F6F", 80);
        fullColorMap.put("#6F3F9F", 81);
        fullColorMap.put("#6F3FCF", 82);
        fullColorMap.put("#6F3FFF", 83);
        fullColorMap.put("#6F6F00", 84);
        fullColorMap.put("#6F6F3F", 85);
        fullColorMap.put("#6F6F6F", 86);
        fullColorMap.put("#6F6F9F", 87);
        fullColorMap.put("#6F6FCF", 88);
        fullColorMap.put("#6F6FFF", 89);
        fullColorMap.put("#6F9F00", 90);
        fullColorMap.put("#6F9F3F", 91);
        fullColorMap.put("#6F9F6F", 92);
        fullColorMap.put("#6F9F9F", 93);
        fullColorMap.put("#6F9FCF", 94);
        fullColorMap.put("#6F9FFF", 95);
        fullColorMap.put("#6FCF00", 96);
        fullColorMap.put("#6FCF3F", 97);
        fullColorMap.put("#6FCF6F", 98);
        fullColorMap.put("#6FCF9F", 99);
        fullColorMap.put("#6FCFCF", 100);
        fullColorMap.put("#6FCFFF", 101);
        fullColorMap.put("#6FFF00", 102);
        fullColorMap.put("#6FFF3F", 103);
        fullColorMap.put("#6FFF6F", 104);
        fullColorMap.put("#6FFF9F", 105);
        fullColorMap.put("#6FFFCF", 106);
        fullColorMap.put("#6FFFFF", 107);
        fullColorMap.put("#9F0000", 108);
        fullColorMap.put("#9F003F", 109);
        fullColorMap.put("#9F006F", 110);
        fullColorMap.put("#9F009F", 111);
        fullColorMap.put("#9F00CF", 112);
        fullColorMap.put("#9F00FF", 113);
        fullColorMap.put("#9F3F00", 114);
        fullColorMap.put("#9F3F3F", 115);
        fullColorMap.put("#9F3F6F", 116);
        fullColorMap.put("#9F3F9F", 117);
        fullColorMap.put("#9F3FCF", 118);
        fullColorMap.put("#9F3FFF", 119);
        fullColorMap.put("#9F6F00", 120);
        fullColorMap.put("#9F6F3F", 121);
        fullColorMap.put("#9F6F6F", 122);
        fullColorMap.put("#9F6F9F", 123);
        fullColorMap.put("#9F6FCF", 124);
        fullColorMap.put("#9F6FFF", 125);
        fullColorMap.put("#9F9F00", 126);
        fullColorMap.put("#9F9F3F", 127);
        fullColorMap.put("#9F9F6F", 128);
        fullColorMap.put("#9F9F9F", 129);
        fullColorMap.put("#9F9FCF", 130);
        fullColorMap.put("#9F9FFF", 131);
        fullColorMap.put("#9FCF00", 132);
        fullColorMap.put("#9FCF3F", 133);
        fullColorMap.put("#9FCF6F", 134);
        fullColorMap.put("#9FCF9F", 135);
        fullColorMap.put("#9FCFCF", 136);
        fullColorMap.put("#9FCFFF", 137);
        fullColorMap.put("#9FFF00", 138);
        fullColorMap.put("#9FFF3F", 139);
        fullColorMap.put("#9FFF6F", 140);
        fullColorMap.put("#9FFF9F", 141);
        fullColorMap.put("#9FFFCF", 142);
        fullColorMap.put("#9FFFFF", 143);
        fullColorMap.put("#CF0000", 144);
        fullColorMap.put("#CF003F", 145);
        fullColorMap.put("#CF006F", 146);
        fullColorMap.put("#CF009F", 147);
        fullColorMap.put("#CF00CF", 148);
        fullColorMap.put("#CF00FF", 149);
        fullColorMap.put("#CF3F00", 150);
        fullColorMap.put("#CF3F3F", 151);
        fullColorMap.put("#CF3F6F", 152);
        fullColorMap.put("#CF3F9F", 153);
        fullColorMap.put("#CF3FCF", 154);
        fullColorMap.put("#CF3FFF", 155);
        fullColorMap.put("#CF6F00", 156);
        fullColorMap.put("#CF6F3F", 157);
        fullColorMap.put("#CF6F6F", 158);
        fullColorMap.put("#CF6F9F", 159);
        fullColorMap.put("#CF6FCF", 160);
        fullColorMap.put("#CF6FFF", 161);
        fullColorMap.put("#CF9F00", 162);
        fullColorMap.put("#CF9F3F", 163);
        fullColorMap.put("#CF9F6F", 164);
        fullColorMap.put("#CF9F9F", 165);
        fullColorMap.put("#CF9FCF", 166);
        fullColorMap.put("#CF9FFF", 167);
        fullColorMap.put("#CFCF00", 168);
        fullColorMap.put("#CFCF3F", 169);
        fullColorMap.put("#CFCF6F", 170);
        fullColorMap.put("#CFCF9F", 171);
        fullColorMap.put("#CFCFCF", 172);
        fullColorMap.put("#CFCFFF", 173);
        fullColorMap.put("#CFFF00", 174);
        fullColorMap.put("#CFFF3F", 175);
        fullColorMap.put("#CFFF6F", 176);
        fullColorMap.put("#CFFF9F", 177);
        fullColorMap.put("#CFFFCF", 178);
        fullColorMap.put("#CFFFFF", 179);
        fullColorMap.put("#FF0000", 180);
        fullColorMap.put("#FF003F", 181);
        fullColorMap.put("#FF006F", 182);
        fullColorMap.put("#FF009F", 183);
        fullColorMap.put("#FF00CF", 184);
        fullColorMap.put("#FF00FF", 185);
        fullColorMap.put("#FF3F00", 186);
        fullColorMap.put("#FF3F3F", 187);
        fullColorMap.put("#FF3F6F", 188);
        fullColorMap.put("#FF3F9F", 189);
        fullColorMap.put("#FF3FCF", 190);
        fullColorMap.put("#FF3FFF", 191);
        fullColorMap.put("#FF6F00", 192);
        fullColorMap.put("#FF6F3F", 193);
        fullColorMap.put("#FF6F6F", 194);
        fullColorMap.put("#FF6F9F", 195);
        fullColorMap.put("#FF6FCF", 196);
        fullColorMap.put("#FF6FFF", 197);
        fullColorMap.put("#FF9F00", 198);
        fullColorMap.put("#FF9F3F", 199);
        fullColorMap.put("#FF9F6F", 200);
        fullColorMap.put("#FF9F9F", 201);
        fullColorMap.put("#FF9FCF", 202);
        fullColorMap.put("#FF9FFF", 203);
        fullColorMap.put("#FFCF00", 204);
        fullColorMap.put("#FFCF3F", 205);
        fullColorMap.put("#FFCF6F", 206);
        fullColorMap.put("#FFCF9F", 207);
        fullColorMap.put("#FFCFCF", 208);
        fullColorMap.put("#FFCFFF", 209);
        fullColorMap.put("#FFFF00", 210);
        fullColorMap.put("#FFFF3F", 211);
        fullColorMap.put("#FFFF6F", 212);
        fullColorMap.put("#FFFF9F", 213);
        fullColorMap.put("#FFFFCF", 214);
        fullColorMap.put("#FFFFFF", 215);
        fullColorMap.put("#1F0000", 216);
        fullColorMap.put("#2F0000", 217);
        fullColorMap.put("#4F0000", 218);
        fullColorMap.put("#5F0000", 219);
        fullColorMap.put("#7F0000", 220);
        fullColorMap.put("#8F0000", 221);
        fullColorMap.put("#AF0000", 222);
        fullColorMap.put("#BF0000", 223);
        fullColorMap.put("#DF0000", 224);
        fullColorMap.put("#EF0000", 225);
        fullColorMap.put("#001F00", 226);
        fullColorMap.put("#002F00", 227);
        fullColorMap.put("#004F00", 228);
        fullColorMap.put("#005F00", 229);
        fullColorMap.put("#007F00", 230);
        fullColorMap.put("#008F00", 231);
        fullColorMap.put("#00AF00", 232);
        fullColorMap.put("#00BF00", 233);
        fullColorMap.put("#00DF00", 234);
        fullColorMap.put("#00EF00", 235);
        fullColorMap.put("#00001F", 236);
        fullColorMap.put("#00002F", 237);
        fullColorMap.put("#00004F", 238);
        fullColorMap.put("#00005F", 239);
        fullColorMap.put("#00007F", 240);
        fullColorMap.put("#00008F", 241);
        fullColorMap.put("#0000AF", 242);
        fullColorMap.put("#0000BF", 243);
        fullColorMap.put("#0000DF", 244);
        fullColorMap.put("#0000EF", 245);
        fullColorMap.put("#1F1F1F", 246);
        fullColorMap.put("#2F2F2F", 247);
        fullColorMap.put("#4F4F4F", 248);
        fullColorMap.put("#5F5F5F", 249);
        fullColorMap.put("#7F7F7F", 250);
        fullColorMap.put("#8F8F8F", 251);
        fullColorMap.put("#AFAFAF", 252);
        fullColorMap.put("#BFBFBF", 253);
        fullColorMap.put("#DFDFDF", 254);
        fullColorMap.put("#EFEFEF", 255);
    }
}