const fs = require('fs');
const readline = require('readline');
const path = require('path');
const {google} = require('googleapis');
const mime = require('mime');
const argv = require('minimist')(process.argv.slice(2));
const delay = require('delay');
const directoryPath = './';

const SCOPES = [
    'https://www.googleapis.com/auth/drive.file',
    'https://www.googleapis.com/auth/drive.metadata',
  ];

const regexPaths = [
     /.*\/package\/.*/g
  ];

//--tag=v0.6.00 
//--token=....
//1UKArdiwYciB-_v5IRJxiCQdzFci_JcSZ

const tag = argv.tag;
const token = argv.token;
const isStable = argv.stable;
const folders = argv._;

if( !tag || !token || !folders || !isStable ){
  console.log('attr missing');
  process.exit(1);
}

upload();

async function upload(){
    try{
        console.log(folders);
        console.log('Build #', tag);
        console.log('Stable? ', isStable);

        const drive = await authorize(decode(token));
        let prepareFiles = scan(directoryPath);

        if(!prepareFiles.length){
            console.log("Nothing to upload ❓");
            process.exit()
        }

        let numRes = await uploadFolders(drive, 0, folders, tag, true, prepareFiles);
        let latestRes = await uploadFolders(drive, 0, folders, 'latest', true, prepareFiles);

        if(isStable && (isStable === true || isStable ==='true')){
            let stableRes = await uploadFolders(drive, 0, folders, 'stable', true, prepareFiles);
            let res = {
                build: numRes,
                latest: latestRes,
                stable: stableRes
            }
            persistFolderIds(res);
        }else{
            let res = {
                build: numRes,
                latest: latestRes
            }
            persistFolderIds(res);
        }

        console.log("Done! 👍");
        process.exit()

    }catch(err){
        console.log("Oops! ❌",err);
        console.warn(err);
        process.exit(1)
    }
}

function decode(b64string) {
  return JSON.parse(Buffer.from(b64string, 'base64'));
}

async function authorize(credentials) {
  const {client_id, client_email, private_key} = credentials;
  const jwtClient = new google.auth.JWT(client_email, null, private_key,SCOPES);

  await jwtClient.authorize();
  return google.drive({version: 'v3', auth:jwtClient});
}

async function uploadFolders(drive, depth, folderIds, dirName, override, prepareFiles){

    let newFolderId ;

    for(let idx in folderIds){
        let fid = folderIds[idx];

        if(override){
            await removeFolder(drive, fid, dirName);
        }

        newFolderId = await createFolder(drive, fid, dirName);
        
        await uploadFilesToSingleFolder(drive, newFolderId, prepareFiles);

    }

    return newFolderId;

}

function persistFolderIds(ids) {
    //Remove previous
    if(fs.existsSync(path.join(__dirname,'drive_id'))){
        console.log('drive_id exist, remove previous')
        fs.unlinkSync(path.join(__dirname,'drive_id'));
    }

    console.log('write to',path.join(__dirname,'drive_id'));
    fs.appendFileSync(path.join(__dirname,'drive_id'), JSON.stringify(ids));
}

async function removeFolder(drive, rootId, dirName){
    let pageToken = null;
    let ids = [];
    do{
      //Remove if exist
      let res = await drive.files.list({
          q: `name = '${dirName}' and '${rootId}' in parents and mimeType = 'application/vnd.google-apps.folder'`,
          fields: 'nextPageToken, files(id, name)',
          spaces: 'drive',
          pageToken: pageToken
      });

      if(res.data.files.length){
          ids.push(...res.data.files);
      }

      if(res.data.nextPageToken){
          console.log('more... ');                
      }

      pageToken = res.data.nextPageToken;

    }while(pageToken!=null);


    if(ids.length){
        for(let idx in ids){
            let f = ids[idx];
            console.log(`found duplicated folder ${f.name} in ${rootId}, remove...`);
            await drive.files.delete({fileId: f.id});  
        }
    }
    
}

async function createFolder(drive, rootId, dirName){
    //Create
    let fileMetadata = {
        'name': dirName,
        parents: [rootId],
        mimeType: 'application/vnd.google-apps.folder',
    }
    let res = await drive.files.create({resource: fileMetadata, fields:'id'});
    
    console.log(`folder ${dirName}(${res.data.id}) created.`);

    return res.data.id;
}

function scan(root){
    let files = [];

    walk(root).forEach(fpath=>{
        regexPaths.some(regex => {  // If file match one pattern.
            if(!fpath.match(regex)) {
                return false;
            }

            let fname = path.basename(fpath);
            let ftype = mime.getType(fname);

            let f = {
                name: fname,
                type: ftype,
                path: fpath
            };

            console.log('prepare: ', f)

            files.push(f);

            return true
        })
    })

    return files;
}

function walk(dir) {
    var results = [];
    var list = fs.readdirSync(dir);
    list.forEach(function(file) {
        file = dir + '/' + file;
        var stat = fs.statSync(file);
        if (stat && stat.isDirectory()) { 
            /* Recurse into a subdirectory */
            results = results.concat(walk(file));
        } else { 
            /* Is a file */
            results.push(file);
        }
    });
    return results;
}

async function uploadFilesToSingleFolder(drive, rootId, files) {

    for(let idx in files){
        let f = files[idx];

        process.stdout.write(f.name + " upload to "+ rootId+" ... ");

        let fileMetadata = {
            'name': f.name,
            parents: [rootId],
        };

        let stream = fs.createReadStream(f.path)

        let media = {
          mimeType: f.type,
          body: stream,
        };

        let res = await drive.files.create({resource: fileMetadata, media: media,fields: 'id'});

        console.log('uploaded');

        await delay(500);
    }
}

