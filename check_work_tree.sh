#!/bin/sh

# https://stackoverflow.com/questions/3878624/how-do-i-programmatically-determine-if-there-are-uncommitted-changes
# Update the index
git update-index -q --ignore-submodules --refresh

# If any unstaged changes in the working tree
if ! git diff-files --quiet --ignore-submodules --
then
    git diff-files --name-status -r --ignore-submodules --
    exit 0
fi

# If any uncommitted changes in the index
if ! git diff-index --cached --quiet HEAD --ignore-submodules --
then
    git diff-index --cached --name-status -r --ignore-submodules HEAD -- 
    exit 0
fi
 
#Nothing to commit
exit 1
