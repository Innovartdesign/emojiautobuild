const fs = require('fs');
const path = require('path');
const readline = require('readline');
const argv = require('minimist')(process.argv.slice(2));
const axios =  require('axios').default;

const slackHook = argv.hook;
const tag = argv.tag;
const changed = argv.changed;
let note = path.join(__dirname,'package','release_note.txt');
let folderId = path.join(__dirname,'drive_id');


if( !slackHook || !tag ){
  console.log('attr missing');
  process.exit(1);
}

sendMessage();

async function sendMessage() {

    try{

		let parentId = getParentId(folderId);
		let msg = getMessage(note);

		let body = {};
        console.log(changed);

        if(parentId.stable){
			body.text =
`<!channel|channel>
封存版本：
https://drive.google.com/open?id=${parentId.build}
最新版本：
https://drive.google.com/open?id=${parentId.latest}
穩定版本：
https://drive.google.com/open?id=${parentId.stable}
${msg}
`;
        }else{
			body.text =
`<!channel|channel>
紀錄版本：
https://drive.google.com/open?id=${parentId.build}
最新版本：
https://drive.google.com/open?id=${parentId.latest}
${msg}

`;
        }

        if(!changed || changed === false || changed === 'false'){
			body.text = body.text+"==沒有檢查到Emoji資料變化==";
        }

		console.log(body);

    	await axios.post(slackHook,body);
		
		clean(folderId);

        console.log("Done! 👍");
        process.exit()
	} catch(err){
        console.log("Oops! ❌",err);
        console.warn(err);
        process.exit(1)
    }

}

function getParentId(p){

	if(!fs.existsSync(p)) return

	let contents = fs.readFileSync(p, 'utf8');
	let ids = JSON.parse(contents);

	console.log('folderId: ', ids);

	return ids;
}

function getMessage(p){

	if(!fs.existsSync(p)) return

	let contents = fs.readFileSync(p, 'utf8');

	return contents;
}

function clean(p){

	if(!fs.existsSync(p)) return

	fs.unlinkSync(p);

}